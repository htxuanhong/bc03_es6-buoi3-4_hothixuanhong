const LIST_TASK = "LIST_TASK";

const luuLocalStorage = (toDoList) => {
    let listJson = JSON.stringify(toDoList);
    localStorage.setItem(LIST_TASK, listJson);
};

function layDanhSachTuLocalStorage() {
    let data = localStorage.getItem(LIST_TASK);

    const items = JSON.parse(data);

    return items || [];
}

function xuatDanhSach(ds) {
    let contenHTML = "";
    let hoaThanhContenHTML = "";

    for (let index = 0; index < ds.length; index++) {
        let newTask = ds[index];

        if (newTask.trangThai == "hoan_thanh") {
            hoaThanhContenHTML += `
            <li>${newTask.moTa}
                 <div class="buttons">
                     <span class="remove" onclick="xoaTask(${index})"><i class="fa fa-trash-alt"></i></span>
                     <span><i class="fa fa-check-circle"></i></span>
                 </div>
            </li>
             `;
        }
        else {
            contenHTML += `
            <li>${newTask.moTa}
                 <div class="buttons">
                     <span class="remove" onclick="xoaTask(${index})"><i class="fa fa-trash-alt"></i></span>
                     <span class="complete" onclick="hoanThanhTask(${index})"><i class="fa fa-check-circle"></i></span>
                 </div>
            </li>
             `;
        }
    }

    document.getElementById("todo").innerHTML = contenHTML;
    document.getElementById("completed").innerHTML = hoaThanhContenHTML;
}

function taiLaiDanhSach() {
    const ds = layDanhSachTuLocalStorage();
    xuatDanhSach(ds);
}

function themTaskLocalStorage(item) {
    const items = layDanhSachTuLocalStorage();

    items.push(item);

    luuLocalStorage(items);
}

function xoaTaskLocalStorage(index) {
    const items = layDanhSachTuLocalStorage();

    items.splice(index, 1);

    luuLocalStorage(items);
}

function hoanThanhTaskLocalStorage(index) {
    const items = layDanhSachTuLocalStorage();

    items[index].trangThai = "hoan_thanh";

    luuLocalStorage(items);
}

taiLaiDanhSach();
