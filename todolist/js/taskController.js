document.getElementById("addItem").addEventListener("click", () => {

  const moTa = document.getElementById("newTask").value;

  const taskItem = new Task(moTa, "chua_hoan_thanh");

  themTaskLocalStorage(taskItem);

  taiLaiDanhSach();

  document.getElementById("newTask").value = "";
})


const xoaTask = (index) => {
  xoaTaskLocalStorage(index);

  taiLaiDanhSach();
}

const hoanThanhTask = (index) => {
  hoanThanhTaskLocalStorage(index);

  taiLaiDanhSach();
}


function sapXepTangDan() {
  const items = layDanhSachTuLocalStorage();
  const sxTang = items.sort((a, b) => a.moTa.localeCompare(b.moTa));

  xuatDanhSach(sxTang)
}

function sapXepGiamDan() {
  const items = layDanhSachTuLocalStorage();
  const sxTang = items.sort((a, b) => b.moTa.localeCompare(a.moTa));

  xuatDanhSach(sxTang)

}

function locTheoChuaHoanThanh() {
  const items = layDanhSachTuLocalStorage();

  const dsChuaHT = items.filter(x => x.trangThai == "chua_hoan_thanh");

  xuatDanhSach(dsChuaHT)
}

function locTheoHoanThanh() {
  const items = layDanhSachTuLocalStorage();

  const dsHT = items.filter(x => x.trangThai == "hoan_thanh");

  xuatDanhSach(dsHT)
}